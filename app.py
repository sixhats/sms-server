from flask import Flask
# from apis import api
from src.apis import api
import config
# from src.core.custom_exceptions import NotFoundError, BusinessRuleConflictError, ValidationError,\
#     NotAuthorizedError, ServerError, ServiceUnavailableError


app = Flask(__name__)
# app.wsgi_app = ProxyFix(app.wsgi_app)


# register error handlers
# def handle_error(error):
#     error_payload = error.to_dict()
#     return error_payload, getattr(error, "code")


# app.register_error_handler(NotFoundError, handle_error)
# app.register_error_handler(NotAuthorizedError, handle_error)
# app.register_error_handler(ValidationError, handle_error)
# app.register_error_handler(BusinessRuleConflictError, handle_error)
# app.register_error_handler(ServiceUnavailableError, handle_error)
# app.register_error_handler(ServerError, handle_error)


# apply configuration settings
config.load(app)
api.init_app(app)


@app.after_request
def after_request(response):
    # response.headers.add('Access-Control-Allow-Origin', '*')
    # response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE')
    return response


if __name__ == '__main__':
    app.run(use_debugger=False, debug=False, use_reloader=True, host='0.0.0.0', port=6543)
