"""
set all the default app settings and get ENV variables from OS env or lambda and store in
application wide configuration object

Flask app object is always accessible during method execution (Request execution ) as flask.current_app
this method below is not part of http request cycle .. this doesnt have access to app object context
Caller who has app context will call this method by passing the "app" instance.


"""


def load(app):
    app.config["RESTPLUS_MASK_SWAGGER"] = False
    app.config['RESTPLUS_MASK_HEADER'] = ''
    app.config["RESTPLUS_VALIDATE"] = True
    app.config["BUNDLE_ERRORS"] = True
    app.config["ERROR_INCLUDE_MESSAGE"] = False
    app.config["ERROR_404_HELP"] = True
    app.config["ENV"] = 'development'
