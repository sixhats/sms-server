from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from toposort import toposort, toposort_flatten

YAML_FILE_PATH = r"/root/Projects/sms-server/scripts/services-order.yaml"

with open(YAML_FILE_PATH) as file_obj:    
    data = load(file_obj, Loader=Loader)
    # output_str = dump(data, Dumper=Dumper, sort_keys=True)
    
    adj_set = {k: set(v) if v else None for k, v in data.items()}
    print(adj_set)
    #topo_sets = list(toposort(adj_set))

    #print(topo_sets)
    # output = list(toposort(output_str))
    # print(output)
    # for item in data:
    #     print(item)
