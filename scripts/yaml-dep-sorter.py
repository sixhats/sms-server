from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

from toposort import toposort

YAML_FILE_PATH = r"/home/kavit/scripts/services-order.yaml"

with open(YAML_FILE_PATH) as file_obj:    
    data = load(file_obj, Loader=Loader)
    adj_set = {}
    for key, item in data.items():
        if key not in adj_set.keys():
           adj_set[key] = set(item['deps']) if item and item.get('deps') else set([])
    
    topo_sets = list(toposort(adj_set))
    print("\nItems grouped by {} can be started or stopped concurrently")
    print("===============\n")
    print(topo_sets)
    print("\nStart Order")
    print("===============\n")
    for start_item_list in topo_sets:
        for start_item in start_item_list:
            print(start_item)
    
    print("\nStop Order")
    print("===============\n")
    for stop_item_list in topo_sets[::-1]:
        for stop_item in reversed(list(stop_item_list)):
            print(stop_item)
    
