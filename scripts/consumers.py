#!/usr/bin/env python
import pika, sys, os, json
from twilio.rest import Client
from multiprocessing import Process


def callback(ch, method, properties, body):
    payload = json.loads(body)
    
    # Your Account Sid and Auth Token from twilio.com/console
    # and set the environment variables. See http://twil.io/secure
    account_sid = "ACdbd8ccbba6341c5a8e1d4c1877659598"
    auth_token = "48ca09f7df13632efec3d1ac52beb80f"
    msg_service_id = payload['msg_service_id']
    content = payload['body']
    to_number = payload['to']

    client = Client(account_sid, auth_token)
    message = client.messages.create(
        body=content,
        messaging_service_sid=msg_service_id,
        to=to_number
    )

    print(" [x] Message sent to Twilio ---- To: %r, sid: %r" % (to_number, message.sid))


class ConsumerOne():
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        queue_name = "ACdbd8ccbba6341c5a8e1d4c1877659598"
        self.channel.queue_declare(queue=queue_name)
        self.channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

        # self.channel.exchange_declare(exchange='e1', exchange_type='direct')
        # self.channel.basic_consume(callback,queue=queue_name,no_ack=False)

    def run(self, args):
        print(' [*] Waiting for messages in Consumer: A. To exit press CTRL+C')
        print(args)
        self.channel.start_consuming()


class ConsumerTwo():
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        queue_name = "ACdbd8ccbba6341c5a8e1d4c1877659598"
        self.channel.queue_declare(queue=queue_name)
        self.channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

        # self.channel.exchange_declare(exchange='e1', exchange_type='direct')
        # self.channel.basic_consume(callback,queue=queue_name,no_ack=False)


    def run(self, args):
        print(' [*] Waiting for messages in Consumer: B. To exit press CTRL+C')
        print(args)
        self.channel.start_consuming()


if __name__ == '__main__':
    try:
        subscriber_list = []
        subscriber_list.append((ConsumerOne(), 'A'))
        subscriber_list.append((ConsumerTwo(), 'B'))

        # execute
        process_list = []
        for sub in subscriber_list:
            process = Process(target=sub[0].run, args=sub[1])
            process.start()
            process_list.append(process)

        # wait for all process to finish
        for process in process_list:
            process.join()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)