import sys, os, redis, json, psycopg2, logging
from select import select
import logging.handlers as handlers


logger = logging.getLogger('redis_job_logs')
logger.setLevel(logging.INFO)

logHandler = handlers.TimedRotatingFileHandler('/root/logs/redis-job.log', when='midnight', interval=1)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logHandler.setLevel(logging.INFO)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)
logger.addHandler(logging.StreamHandler())

def wait(conn):
	while True:
		state = conn.poll()
		if state == psycopg2.extensions.POLL_OK:
			break
		elif state == psycopg2.extensions.POLL_WRITE:
			select([], [conn.fileno()], [])
		elif state == psycopg2.extensions.POLL_READ:
			select([conn.fileno()], [], [])
		else:
			raise psycopg2.OperationalError("poll() returned %s" % state)

class SendingStatus(object):
	Queued = 1
	Sent = 2
	Delivered = 3
	Failed = 4

def update_sent_status(cursor, data):
	postgreSQL_Query = """UPDATE tbl_sms_sent_report 
		SET 
			status=%(status)s, 
			sent_time=%(sent_time)s,
			provider_sid=%(provider_sid)s,
			provider_resp=%(error_msg)s
		WHERE internal_message_id = %(internal_message_id)s
	;"""
	bulk_data = [item for key, item in data.items()]
	cursor.executemany(postgreSQL_Query, bulk_data)



if __name__ == '__main__':    

	db = psycopg2.connect(host='159.89.137.110', port=5432, database='sms_system_db', user='postgres', password='Saquib@2020',)
	cursor = db.cursor()
	logger.info("Connected to PostgreSQL")
	
	redis_conn = redis.Redis(host='localhost', port=6379, db=0, password='Admin@Redis', charset="utf-8", decode_responses=True)
	all_keys = redis_conn.keys()
	# print(len(all_keys))
	data = {}
	for item in all_keys:
		payload = json.loads(redis_conn.get(item))
		# print(payload)
		internal_msgid = payload.get("internal_msgid")
		sent_time = payload.get("sent_time")
		if  internal_msgid and internal_msgid not in data.keys():				
			data[internal_msgid] = {
				"internal_message_id": internal_msgid,
				"sent_time": sent_time,
				"provider_sid": "",
				"status": "",
				"error_msg": ""
			}				
			if payload.get("provider_sid"):
				data[internal_msgid]["provider_sid"] = payload["provider_sid"]
				data[internal_msgid]["status"] = SendingStatus.Sent
			elif payload.get("error_msg"):
				data[internal_msgid]["status"] = SendingStatus.Failed
				data[internal_msgid]["error_msg"] = json.dumps(payload["error_msg"])
		
		if len(data.keys()) == 100:
			update_sent_status(cursor, data)
			wait(db)
			updated_rows = cursor.rowcount
			logger.info("Updating Table tbl_sms_sent_report. Update Row Count: %s" %(updated_rows))
			db.commit()
			data.clear()
	if len(data.keys()) > 0:
		update_sent_status(cursor, data)
		wait(db)
		updated_rows = cursor.rowcount
		logger.info("Updating Table tbl_sms_sent_report. Update Row Count: %s" %(updated_rows))
		db.commit()
		data.clear()
	if len(all_keys) > 0:
		redis_conn.delete(*all_keys)

	cursor.close()
	db.close()
	logger.info("Disconnected from PostgreSQL")
	