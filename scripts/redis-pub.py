import redis, json
from uuid import uuid4


redis_conn = redis.Redis(host='localhost', port=6379, db=0, password='Admin@Redis', charset="utf-8", decode_responses=True)

def pub():
	msgid = str(uuid4())
	data = {
		"message": "hello",
		"from": "50505",
		"to": "9870112233"
	}
	redis_conn.set(msgid, json.dumps(data))
	redis_conn.publish("broadcast", msgid)
	

if __name__ == "__main__":
	pub()
