
import sys, os, redis, json, psycopg2, logging
from multiprocessing import Process
from select import select
import logging.handlers as handlers


logger = logging.getLogger('redis_job_logs')
logger.setLevel(logging.INFO)

logHandler = handlers.TimedRotatingFileHandler('/opt/sms-server/logs/redis-job.log', when='midnight', interval=1)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logHandler.setLevel(logging.INFO)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)
logger.addHandler(logging.StreamHandler())

def wait(conn):
	while True:
		state = conn.poll()
		if state == psycopg2.extensions.POLL_OK:
			break
		elif state == psycopg2.extensions.POLL_WRITE:
			select([], [conn.fileno()], [])
		elif state == psycopg2.extensions.POLL_READ:
			select([conn.fileno()], [], [])
		else:
			raise psycopg2.OperationalError("poll() returned %s" % state)


class SendingStatus(object):
	Queued = 1
	Sent = 2
	Delivered = 3
	Failed = 4


def update_sent_status(cursor, data):
	postgreSQL_Query = """UPDATE tbl_sms_sent_report 
		SET 
			status=%(status)s, 
			sent_time=%(sent_time)s,
			provider_sid=%(provider_sid)s,
			provider_resp=%(error_msg)s
		WHERE internal_message_id = %(internal_message_id)s
	;"""
	cursor.execute(postgreSQL_Query, data)

def deduct_user_balance(cursor, user_id, total_cost):
	postgreSQL_select_Query = "UPDATE tbl_users SET balance = balance - %s where id = %s;"
	cursor.execute(postgreSQL_select_Query, (total_cost, user_id))	


def processSentData(payload, cursor, db):
	if payload:
		internal_msgid = payload.get("internal_msgid")
		sent_time = payload.get("sent_time")
		if internal_msgid:				
			data = {
				"internal_message_id": internal_msgid,
				"sent_time": sent_time,
				"provider_sid": "",
				"status": "",
				"error_msg": ""
			}				
			if payload.get("provider_sid"):
				data["provider_sid"] = payload["provider_sid"]
				data["status"] = SendingStatus.Sent
				user_id = payload["user_id"]
				sms_cost = payload["sms_cost"]
				wait(db)
				deduct_user_balance(cursor, user_id, sms_cost)
				db.commit()
			elif payload.get("error_msg"):
				data["status"] = SendingStatus.Failed
				data["error_msg"] = json.dumps(payload["error_msg"])
			
			wait(db)
			update_sent_status(cursor, data)
			db.commit()
			return data
	return False

	
def sub(name: str):

	# # for development server
	# redis_conn = redis.Redis(host='localhost', port=6379, db=0, password='Admin@Redis', charset="utf-8", decode_responses=True)
	# db = psycopg2.connect(host='164.90.152.225', port=5432, database='sms_system_db', user='postgres', password='Saquib@2020',)
	

	# for production server 
	redis_conn = redis.Redis(host='localhost', port=6379, db=0, password='Admin@Redis', charset="utf-8", decode_responses=True)
	db = psycopg2.connect(host='gateway.inboxed.co', port=5432, database='sms_system_db', user='django_api', password='jK@571Vv8w',)

	
	cursor = db.cursor()
	logger.info("Connected to PostgreSQL")
		
	all_keys = redis_conn.keys()
	for msgid in all_keys:
		payload = json.loads(redis_conn.get(msgid))
		processedData = processSentData(payload, cursor, db)
		if processedData:
			logger.info("SMS Sent Updated [internal_message_id: %s]" % (msgid,))
		else:
			logger.info("SMS Sent Failed [internal_message_id: %s] [payload: %s]" % (msgid, payload))

	if len(all_keys) > 0:
		redis_conn.delete(*all_keys)
		
	pubsub = redis_conn.pubsub()
	pubsub.subscribe("broadcast")
	logger.info('Starting listening messages in loop')
	
	try:
		for message in pubsub.listen():
			if message.get("type") == "message":
				msgid = message.get("data")
				payload = json.loads(redis_conn.get(msgid)) if redis_conn.get(msgid) else {}
				processedData = processSentData(payload, cursor, db)
				if processedData:
					logger.info("SMS Sent Update Success [internal_message_id: %s]" % (msgid,))
				else:
					logger.info("SMS Sent Update Failed [internal_message_id: %s] [payload: %s]" % (msgid, payload))
				if payload:
					redis_conn.delete(msgid)
	
	except KeyboardInterrupt:	
		logger.info('Keyboard Interrupt')
		cursor.close()
		db.close()
		logger.info("Disconnected from PostgreSQL")
		
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)



if __name__ == "__main__":
	Process(target=sub, args=("redis-reader",)).start()


