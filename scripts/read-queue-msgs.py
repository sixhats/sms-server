#!/usr/bin/env python
import logging, pika, sys, os, json, psycopg2
from datetime import datetime
from select import select
import logging.handlers as handlers
from twilio.rest import Client
import twilio
import multiprocessing 
import os
import requests, base64


logger = logging.getLogger('messages_logs')
logger.setLevel(logging.INFO)

logHandler = handlers.TimedRotatingFileHandler('/opt/sms-server/logs/messages.log', when='midnight', interval=1)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logHandler.setLevel(logging.INFO)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)
logger.addHandler(logging.StreamHandler())


def wait(conn):
	while True:
		state = conn.poll()
		if state == psycopg2.extensions.POLL_OK:
			break
		elif state == psycopg2.extensions.POLL_WRITE:
			select([], [conn.fileno()], [])
		elif state == psycopg2.extensions.POLL_READ:
			select([conn.fileno()], [], [])
		else:
			raise psycopg2.OperationalError("poll() returned %s" % state)

class SendingStatus(object):
	Queued = 1
	Sent = 2
	Delivered = 3
	Failed = 4

class RouteType(object):
	JASMIN = 1
	MOBINITI = 2
	SMSGORILLA = 3
	BlastMeOnline = 4
	TWILIO = 5

def get_route_accounts(cursor):
	# sql = '''SELECT * from EMP INNER JOIN CONTACT ON EMP.CONTACT = CONTACT.ID'''
	postgreSQL_select_Query = "select id, rid, worker_count, status, admin_alias from tbl_sms_route_account where status=1"
	cursor.execute(postgreSQL_select_Query)
	results = cursor.fetchall()
	return results

def update_sent_status(cursor, data):
	postgreSQL_Query = """UPDATE tbl_sms_sent_report 
		SET 
			status=%(status)s, 
			sent_time=%(sent_time)s,
			provider_sid=%(provider_sid)s,
			provider_resp=%(error_msg)s
		WHERE internal_message_id = %(internal_message_id)s
	;"""
	cursor.execute(postgreSQL_Query, data)

def deduct_user_balance(cursor, user_id, total_cost):
	postgreSQL_select_Query = "UPDATE tbl_users SET balance = balance - %s where id = %s;"
	cursor.execute(postgreSQL_select_Query, (total_cost, user_id))	


class InboundSMS:
	
	def __init__(self, logger_obj, payload):
		self.connection = psycopg2.connect(host='164.90.152.225', port=5432, database='sms_system_db', user='postgres', password='Saquib@2020',)
		self.logger = logger_obj
		# self.logger.info("Connected to PostgreSQL Server")
		self.payload = payload		
		# timeStr = time.ctime()
		# connect to postgres
		# self.logger.info("Task started: {} ".format(timeStr,))
		# self.logger.info("[x] Message Received in Worker: %r ---- To: %r, From: %r, Body: %r" % (worker_name, payload["To"], payload["From"], payload["Body"]))
		self.cursor = self.connection.cursor()
		
	
	def sendSMS(self, data):
		headers = {
			'Content-Type':'application/json',
		}
		
		# Development URL
		HTTP_API_SEND_URL = "http://143.198.111.248:6543/send"
		
		# # Production URL
		# HTTP_API_SEND_URL = "https://targ1-api.inboxed.co/send"
		
		resp = requests.post(HTTP_API_SEND_URL, data=json.dumps(data), headers=headers)
		if resp.status_code==200 and resp.json():
			response_dict = resp.json()
			return response_dict['data']
		return False
	
	def callRasa(self, data):
		headers = {
			'Content-Type':'application/json',
		}
		
		# Development URL
		HTTP_API_SEND_URL = "http://165.232.150.85:5005/webhooks/myio/webhook"
		
		# # Production URL
		# HTTP_API_SEND_URL = "https://targ1-api.inboxed.co/send"
		
		resp = requests.post(HTTP_API_SEND_URL, data=json.dumps(data), headers=headers)
		if resp.status_code==200 and resp.json():
			response_dict = resp.json()
			return response_dict
		return False


	def db_close(self):
		if self.connection is not None:
			self.cursor.close()
			self.connection.close()
		# self.logger.info("Disconnected from PostgreSQL")
		# timeStr = time.ctime()
		# self.logger.info("Task Finished: {} \n".format(timeStr,))
	
	def insert_inbound_sms(self, data):
		self.cursor.execute("""
			INSERT INTO tbl_sms_inbound_report(
				sms_message_sid,
				num_media,
				sms_sid,
				from_number,
				from_country_code,
				from_state,
				from_city,
				from_zip,
				to_number,
				to_country_code,
				to_state,
				to_city,
				to_zip,
				sms_status,
				message,
				messaging_service_sid,
				message_sid,
				account_sid,
				num_segments,
				api_version,
				reply_sent,
				sent_report_id,
				created,
				modified
			)
			values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (
				data['SmsMessageSid'], 
				data['NumMedia'], 
				data['SmsSid'], 
				data['From'], 
				data['FromCountry'], 
				data['FromState'], 
				data['FromCity'], 
				data['FromZip'], 
				data['To'], 
				data['ToCountry'], 
				data['ToState'], 
				data['ToCity'], 
				data['ToZip'], 
				data['SmsStatus'], 
				data['Body'], 
				data['MessagingServiceSid'], 
				data['MessageSid'], 
				data['AccountSid'], 
				data['NumSegments'], 
				data['ApiVersion'],
				data['reply_sent'], 
				data['sent_report_id'],
				datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
				datetime.now().strftime("%Y-%m-%d %H:%M:%S")
			)
		)
		self.connection.commit()

	def insert_sent_report(self, data):
		self.cursor.execute("""
			INSERT INTO tbl_sms_sent_report(
				internal_message_id,
				route_id,
				sending_cost,
				surcharge_cost,
				number_from,
				phone,
				carrier,
				message,
				is_reply,
				short_url,
				sent_url_group_id,
				sent_domain_group_id,
				sent_template_group_id,
				template_id,
				domain_id,
				offer_url_id,
				subscriber_id,
				sms_list_id,
				campaign_id,
				user_id,
				status,
				created,
				modified
			)
			values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", (
				data['internal_message_id'], 
				data['route_id'], 
				data['sending_cost'], 
				data['surcharge_cost'], 
				data['number_from'], 
				data['phone'], 
				data['carrier'], 
				data['message'], 
				data['is_reply'], 
				data['short_url'], 
				data['sent_url_group_id'], 
				data['sent_domain_group_id'], 
				data['sent_template_group_id'], 
				data['template_id'], 
				data['domain_id'], 
				data['offer_url_id'], 
				data['subscriber_id'], 
				data['sms_list_id'], 
				data['campaign_id'], 
				data['user_id'],
				data['status'],
				datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
				datetime.now().strftime("%Y-%m-%d %H:%M:%S")
			)
		)
		self.connection.commit()

	def get_recepeints_by_id(self, subscriber_id):
		postgreSQL_select_Query = """SELECT id, first_name, last_name, phone, 
			carrier, email, sms_list_id, us_zip, us_state, us_city, city, 
			state, custom_1, custom_2, custom_3
			FROM tbl_subscribers 
			WHERE id = %s;
		"""
		self.cursor.execute(postgreSQL_select_Query, (subscriber_id,))	
		results = self.cursor.fetchone()
		if results:
			return {
				'id': results[0],
				'first_name': results[1],
				'last_name': results[2],
				'phone': results[3],
				'carrier': results[4],
				'email': results[5],
				'sms_list_id': results[6],
				'us_zip': results[7],
				'us_state': results[8],
				'us_city': results[9],
				'city': results[10],
				'state': results[11],
				'custom_1': results[12],
				'custom_2': results[13],
				'custom_3': results[14],
			} 
		return None	

	def get_sent_report_record(self, from_number, to_number):
		postgreSQL_select_Query = """SELECT t1.id, t1.number_from, t1.phone, t1.short_url, t1.campaign_id,
			t1.sending_cost, t1.surcharge_cost, t1.sent_url_group_id, t1.sent_domain_group_id, 
			t1.sent_template_group_id, t1.template_id, t1.offer_url_id, t1.domain_id, t1.subscriber_id,
			t1.sms_list_id, t1.user_id, t1.route_id, t1.sent_time, t2.is_reply_camp
			FROM tbl_sms_sent_report t1 
			INNER JOIN tbl_sms_campaigns t2 on t2.id = t1.campaign_id
			WHERE t1.number_from = %s and t1.phone = %s and t2.is_reply_camp = 1
			ORDER BY t1.sent_time DESC
		;"""
		self.cursor.execute(postgreSQL_select_Query, (to_number, from_number))	
		results = self.cursor.fetchone()
		if results:
			return {
				'id': results[0],
				'number_from': results[1],
				'phone': results[2],
				'short_url': results[3],
				'campaign_id': results[4],
				'sending_cost': results[5],
				'surcharge_cost': results[6],
				'sent_url_group_id': results[7],
				'sent_domain_group_id': results[8],
				'sent_template_group_id': results[9],
				'template_id': results[10],
				'offer_url_id': results[11],
				'domain_id': results[12],
				'subscriber_id': results[13],
				'sms_list_id': results[14],
				'user_id': results[15],
				'route_id': results[16],
				'sent_time':results[17],
			}
		return None

	def get_route_account_by_id(self, route_id):
		postgreSQL_select_Query = """SELECT id, rid, sender_type, from_addr, 
			api_url, api_key, api_secret
			FROM tbl_sms_route_account
			WHERE id = %s;
		"""
		self.cursor.execute(postgreSQL_select_Query, (route_id,))	
		results = self.cursor.fetchone()
		if results:
			return {
				'id': results[0],
				'rid': results[1],
				'sender_type': results[2], 
				'from_addr' : results[3],
				'api_url' : results[4],
				'api_key' : results[5],
				'api_secret' : results[6]
			}
		return None
	
	def main(self):
		to_number = ''.join([i for i in self.payload["To"] if i.isdigit()]) 
		from_number = ''.join([i for i in self.payload["From"] if i.isdigit()]) 
		# MessageSid = payload["MessageSid"]
		sent_report_record = self.get_sent_report_record(from_number, to_number)
		if sent_report_record:
			recepeint_record = self.get_recepeints_by_id(sent_report_record["subscriber_id"])
			route_record = self.get_route_account_by_id(sent_report_record["route_id"])			
			if recepeint_record and route_record:
				
				data= {
					"sender": recepeint_record["id"],
					"message":self.payload["Body"],
					"metadata":{
						"offer_url": sent_report_record["short_url"],
						"first_name": recepeint_record["first_name"],
						"last_name": recepeint_record["last_name"],
					},
				}
				response_dict = self.callRasa(data)
				send_flag = 0
				self.payload["reply_sent"] = 0
				self.payload["sent_report_id"] = sent_report_record["id"]
				sms_cost = sent_report_record["sending_cost"] + sent_report_record["surcharge_cost"] 
				if len(response_dict) > 0 and response_dict[0].get("custom"):
					send_flag = response_dict[0]["custom"]["send_flag"]
					if send_flag == 1:
						self.payload["reply_sent"] = send_flag
						text = response_dict[0]["custom"]["text"]
						appstrcut = {
							"internal_message_id": "",
							"route_id": sent_report_record["route_id"],
							"sending_cost": sent_report_record["sending_cost"],
							"surcharge_cost": sent_report_record["surcharge_cost"],
							"number_from": route_record['from_addr'],
							"phone": from_number,
							"carrier": recepeint_record["carrier"],
							"message": text,
							"is_reply": 1,
							"short_url": sent_report_record["short_url"],
							"sent_url_group_id": sent_report_record["sent_url_group_id"],
							"sent_domain_group_id": sent_report_record["sent_domain_group_id"],
							"sent_template_group_id": sent_report_record["sent_template_group_id"],
							"template_id": sent_report_record["template_id"],
							"domain_id": sent_report_record["domain_id"],
							"offer_url_id": sent_report_record["offer_url_id"],
							"subscriber_id": sent_report_record["subscriber_id"],
							"sms_list_id": sent_report_record["sms_list_id"],
							"campaign_id": sent_report_record["campaign_id"],
							"status": 1,
							"user_id": sent_report_record["user_id"],
						}

						send_sms_dict = {
							"from": route_record['from_addr'],
							"provider_type": route_record['sender_type'],
							"sid": route_record['rid'],
							"user_id": sent_report_record["user_id"],
							"username": route_record['api_key'],
							"password": route_record['api_secret'],
							"api_url": route_record['api_url'], 
							"messages": [{
								"to_number": from_number,
								"content": text,
								"sms_cost": sms_cost,
							}]
						}
						
						sent_resp = self.sendSMS(send_sms_dict)
						if sent_resp:
							for each_resp in sent_resp['messages']:
								self.logger.info("[x] Message Sent to: {}, internal_message_id:{}, Body: {}".format(each_resp['to_number'], each_resp['msgid'], each_resp['content'] ))	
								appstrcut['internal_message_id'] = each_resp['msgid']
						self.insert_sent_report(appstrcut)
				self.insert_inbound_sms(self.payload)

		self.db_close()
		

def processSentData(payload, cursor, db):
	if payload:
		internal_msgid = payload.get("internal_msgid")
		sent_time = payload.get("sent_time")
		if internal_msgid:				
			data = {
				"internal_message_id": internal_msgid,
				"sent_time": sent_time,
				"provider_sid": "",
				"status": "",
				"error_msg": ""
			}				
			if payload.get("provider_sid"):
				data["provider_sid"] = payload["provider_sid"]
				data["status"] = SendingStatus.Sent
				user_id = payload["user_id"]
				sms_cost = payload["sms_cost"]
				wait(db)
				deduct_user_balance(cursor, user_id, sms_cost)
				db.commit()
			elif payload.get("error_msg"):
				data["status"] = SendingStatus.Failed
				data["error_msg"] = json.dumps(payload["error_msg"])
			
			wait(db)
			update_sent_status(cursor, data)
			db.commit()
			return data
	return False


class OutboundSMS:
	
	def __init__(self, channel, logger_obj, worker_name, payload):
		
		self.channel = channel
		self.logger = logger_obj
		self.worker_name = worker_name
		self.payload = payload	
		self.msgid = payload['msgid']
		self.user_id = payload['user_id']
		self.sms_cost = payload['sms_cost']
		self.provider_type = payload['provider_type']
		self.api_URL = payload['api_url']
		self.content = payload['body']
		self.to_number = payload['to']
		self.from_number = payload['from']
		self.username = payload['username']
		self.password = payload['password']	
		self.data = {
			"internal_message_id": payload['msgid'],
			"sent_time": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
			"status": SendingStatus.Failed,
			"provider_sid": "",
			"error_msg": ""
		}
		
	def sendToSMSGorillaRoute(self):
		
		api_URL = "https://api.smsgorilla.com/messages"
		usrPass = self.username + ':' + self.password
		encoded_u = base64.b64encode(usrPass.encode()).decode()
		headers = {
			"Authorization" : "Basic %s" % encoded_u,
			"Content-Type": "application/x-www-form-urlencoded"
		}
		params =  {
			"body": self.content,
			"from": self.from_number,
			"to": self.to_number,
		}
		msg_resp =requests.post(api_URL, headers=headers, data=params)
		if msg_resp.status_code == 200:
			resp_sid = msg_resp.json()['result']['id'] if msg_resp.json().get('result') and msg_resp.json()['result'].get('id') else 'ERROR'
			self.data["provider_sid"] = resp_sid
			self.data["status"] = SendingStatus.Sent
			# data['provider_balance'] = msg_resp.json()['result']['balance'] if msg_resp.json().get('result') and msg_resp.json()['result'].get('balance') else None
			self.logger.info("[x] Message sent to SMSGORILLA from Worker: %r ---- To: %r, sid: %r, msgid: %r" % (self.worker_name, self.to_number, resp_sid, self.msgid))
		else:
			self.data['error_msg'] = msg_resp.json()['message']
			self.logger.info("[x] Message sent to SMSGORILLA from Worker: %r ---- To: %r, msgid: %r, Error Msg: %r" % (self.worker_name, self.to_number, self.msgid, msg_resp.json()['message']))
			self.logger.info("[x] sent-data: "+ str(params))

	def sendToJasminRoute(self):
		baseStr = self.username + ':' + self.password
		message_bytes = baseStr.encode()
		base64Str = base64.b64encode(message_bytes)
		encodedStr = base64Str.decode()
		headers = {
			"content-type": "application/json", 
			"accept": "application/json",
			"Authorization": "Basic %s" % encodedStr
		}				
		params =  {
			"content": self.content,
			"from": self.from_number,
			"to": self.to_number,
		}
		resp = requests.post(self.api_URL, data=json.dumps(params),  headers=headers)
		if resp.status_code == 200:
			resp_data = resp.json()['data']
			resp_list = resp_data.split('"')
			if len(resp_list) > 0 and resp_list[0].startswith('Success') and len(resp_list[1]) == 36:
				resp_sid = resp_list[1]
				self.data["provider_sid"] = resp_sid
				self.data["status"] = SendingStatus.Sent
				self.logger.info("[x] Message sent to Jasmin from Worker: %r ---- To: %r, sid: %r, msgid: %r" % (self.worker_name, self.to_number, resp_sid, self.msgid))
			else:
				self.data["error_msg"] = resp_data
				self.logger.info("[x] Message sent to Jasmin from Worker: %r ---- To: %r, msgid: %r, Error Msg: %r" % (self.worker_name, self.to_number, self.msgid, resp.json()))
				self.logger.info("sent-data: "+ str(params))
		else:
			self.data["error_msg"] = resp.json()
			self.logger.info("[x] Message sent to Jasmin from Worker: %r ---- To: %r, msgid: %r, Error Msg: %r" % (self.worker_name, self.to_number, self.msgid, resp.json()))
			self.logger.info("sent-data: "+ str(params))

	def sendToBMORoute(self,):
		api_URL = "https://blastme.online/api/v1/sms/text"	
		baseStr = self.username + ':' + self.password
		message_bytes = baseStr.encode()
		base64Str = base64.b64encode(message_bytes)
		encodedStr = base64Str.decode()

		headers = {
			"content-type": "application/json", 
			"Authorization": "Basic %s" % encodedStr
		}
		params =  {
			"text": self.content,
			#"from": self.from_number,
			"to": self.to_number,
		}
		msg_resp = requests.post(api_URL, data=json.dumps(params),  headers=headers, verify=False)
		if msg_resp.status_code == 200:
			resp_data = msg_resp.json()
			# {'message_id': 'M017181003-000000199331', 'to': '15624760078', 'from': '17181003', 'submit_status': 'OK', 'sms_count': 1, 'created_date': '2021-06-08T11:46:07.632365Z'}
			if resp_data.get("message_id") and resp_data.get("submit_status") == "OK":
				resp_sid = resp_data['message_id']
				self.data["provider_sid"] = resp_sid
				self.data["status"] = SendingStatus.Sent
				self.logger.info("[x] Message sent to BMO from Worker: %r ---- To: %r, sid: %r, msgid: %r" % (self.worker_name, self.to_number, resp_sid, self.msgid))
			else:
				self.data["error_msg"] = msg_resp.json()
				self.logger.info("[x] Message sent to BMO from Worker: %r ---- To: %r, msgid: %r, Error Msg: %r" % (self.worker_name, self.to_number, self.msgid, msg_resp.json()))
				self.logger.info("[x] sent-data: "+ str(params))
		else:
			self.data["error_msg"] = msg_resp.json()
			self.logger.info("[x] Message sent to BMO from Worker: %r ---- To: %r, msgid: %r, Error Msg: %r" % (self.worker_name, self.to_number, self.msgid, msg_resp.json()))
			self.logger.info("[x] sent-data: "+ str(params))

	def sendToTwilioRoute(self,):
		try:
			client = Client(self.username, self.password)
			message = client.messages.create(
				body=self.content,
				from_=self.from_number,
				to=self.to_number
			)
			if message.sid:
				self.data["provider_sid"] = message.sid
				self.data["status"] = SendingStatus.Sent
				self.logger.info("[x] Message sent to Twilio from Worker: %r ---- To: %r, sid: %r, msgid: %r" % (self.worker_name, self.to_number, message.sid, self.msgid))
		
		except twilio.base.exceptions.TwilioRestException as e:
			params =  {
				"text": self.content,
				"from": self.from_number,
				"to": self.to_number,
			}
			self.data["error_msg"] = str(e.msg)
			self.logger.info("[x] Message sent to Twilio from Worker: %r ---- To: %r, msgid: %r, Error Msg: %r" % (self.worker_name, self.to_number, self.msgid, e.msg))
			self.logger.info("[x] sent-data: "+ str(params))

	def main(self):
		if self.provider_type == RouteType.SMSGORILLA:
			self.sendToSMSGorillaRoute()
		elif self.provider_type == RouteType.JASMIN:
			self.sendToJasminRoute()
		elif self.provider_type == RouteType.BlastMeOnline:
			self.sendToBMORoute()
		elif self.provider_type == RouteType.TWILIO:
			self.sendToTwilioRoute()

		self.channel.queue_declare(queue="dlr-update")
		self.channel.basic_publish(exchange='', routing_key="dlr-update", body=json.dumps(self.data))


def outbound_multiprocessing_func(worker_name, queue_name):
	connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
	channel = connection.channel()
	channel.queue_declare(queue=queue_name)

	def callback(ch, method, properties, body):
		payload = json.loads(body)
		outbound_task = OutboundSMS(channel, logger ,worker_name, payload)
		outbound_task.main()
		ch.basic_ack(delivery_tag = method.delivery_tag)

	channel.basic_consume(queue=queue_name, on_message_callback=callback)
	logger.info(" [*] Waiting for messages in Worker: %r, Queue-Name: %r. To exit press CTRL+C" %(worker_name, queue_name))
	channel.start_consuming()

def inbound_multiprocessing_func(worker_name, queue_name):
	connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
	channel = connection.channel()
	channel.queue_declare(queue=queue_name)

	def callback(ch, method, properties, body):		
		payload = json.loads(body)
		to_number = ''.join([i for i in payload["To"] if i.isdigit()]) 
		from_number = ''.join([i for i in payload["From"] if i.isdigit()]) 
		Body = payload["Body"]
		logger.info("[x] Message Received in Worker: %r ---- To: %r, From: %r, Body: %r" % (worker_name, to_number, from_number, Body))
		inbound_task = InboundSMS(logger ,payload)
		inbound_task.main()

	channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
	logger.info(" [*] Waiting for messages in Worker: %r, Queue-Name: %r. To exit press CTRL+C" %(worker_name, queue_name))
	channel.start_consuming()


if __name__ == '__main__':
	try:
		# pool = Pool(WORKER_SIZE)
		processes = []

		# # for development server
		# db = psycopg2.connect(host='164.90.152.225', port=5432, database='sms_system_db', user='postgres', password='Saquib@2020',)

		# for production server 
		db = psycopg2.connect(host='gateway.inboxed.co', port=5432, database='sms_system_db', user='django_api', password='jK@571Vv8w',)

		cursor = db.cursor()
		logger.info("Connected to PostgreSQL\n")

		route_accounts = get_route_accounts(cursor)
		
		for route in route_accounts:
			route_rid = route[1]
			worker_count = route[2] 
			admin_alias = route[4] if route[4] else "" 
			count = 1
			for item in range(0, worker_count):
				worker_name = admin_alias+"-"+str(count)
				p = multiprocessing.Process(target=outbound_multiprocessing_func, args=(str(worker_name),  route_rid))
				processes.append(p)
				p.start()
				count += 1    
	
		# inboundQueue = ["inbound-sms", 3]
		# count = 1
		# for item in range(0, inboundQueue[1]):
		# 	worker_name = inboundQueue[0]+"-"+str(count)
		# 	p = multiprocessing.Process(target=inbound_multiprocessing_func, args=(str(worker_name),  inboundQueue[0]))
		# 	processes.append(p)
		# 	p.start()
		# 	count += 1

		for process in processes:
			process.join()

		# multiprocessing_func('Main Worker')
	except KeyboardInterrupt:
		logging.info('Interrupted')

		cursor.close()
		db.close()
		logger.info("Disconnected from PostgreSQL")
		# p.terminate()
		# q.terminate()
		try:
			sys.exit(0)
		except SystemExit:
			os._exit(0)
