#!/usr/bin/env python
import pika

def main():
    # Establish a connection to broker
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()

    # create/declare a queue
    channel.queue_declare(queue='test-queue')

    # publish a message to the queue
    channel.basic_publish(exchange='', routing_key='test-queue', body='Hello World 4!')

    # close the connection
    connection.close()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)