from flask_restplus import Api
from src.apis.sendSMS import api as sendSMSAPI

api = Api(title="SMS Server API")
api.add_namespace(sendSMSAPI, path="/")
