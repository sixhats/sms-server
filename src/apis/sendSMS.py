import logging, time, datetime, json, pika, logging.handlers as handlers
from uuid import uuid4
from flask_restplus import Namespace, Resource, fields, reqparse

# from core.custom_exceptions import NotFoundError, BusinessRuleConflictError
from src.core.custom_fields import validate_custom_fields_in_payload, PhoneNumberField, PasswordField
# from core.custom_fields import validate_custom_fields_in_payload
# from core import success_msgs, error_msgs

logger = logging.getLogger('api_logs')
logger.setLevel(logging.INFO)

logHandler = handlers.TimedRotatingFileHandler('/opt/sms-server/logs/sms-http-api.log', when='midnight', interval=1)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logHandler.setLevel(logging.INFO)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)
# logger.addHandler(logging.StreamHandler())


api = Namespace("Send-Receive-SMS")

SendSMSResponseBodySchema = api.model("SendSMSResponseBodySchema", {
	"msgid": fields.String(required=True, min_length=1, max_length=100, example="3cc559b8-75cb-48c1-87de-0f5e14f3f073"),
	"to_number": PhoneNumberField(required=True, min_length=4, max_length=15, description="to number", example="19899998568"),
	"content": fields.String(required=True, example='Test Msg'),
	# "msg_service_id": fields.String(required=False, min_length=1, max_length=100, example="MG55e6f23edd6b6cfee3936e816d6e9d84"),
})

SendSMSSchema = api.model("SendSMS", {
	# "request_id": fields.String(required=True, min_length=1, max_length=100, example="c2bda6a2-24e0-4b58-9c37-d3260e1080da"),
	"messages": fields.List(fields.Nested(SendSMSResponseBodySchema, required=True, skip_none=True), skip_none=True)
})

SendSMSResponse = api.model("SendSMSResponse", {
	"status": fields.String(required=True, default="Success", example="Success"),
	"code": fields.String(required=True, default="200", example="200"),
	"message": fields.String(required=True, default="Message sent successfully", example="Message sent successfully"),
	"data": fields.Nested(SendSMSSchema, required=True, skip_none=True)
})


SendSMSBodySchema = api.model("SendSMSBodySchema", {
	"to_number": PhoneNumberField(required=True, min_length=4, max_length=15, description="to number", example="16364758053"),
	"content": fields.String(required=True, example='Test Msg'),
	"sid": fields.String(required=False, min_length=1, max_length=100, example="ACdbd8ccbba6341c5a8e1d4c1877659598"),
	"from": fields.String(required=False, min_length=1, max_length=100, example="50505"),
	# "sms_cost": fields.Float(required=True),
	# "msg_service_id": fields.String(required=False, min_length=1, max_length=100, example="MG55e6f23edd6b6cfee3936e816d6e9d84"),
})

SendSMSPayload = api.model("SendSMSPayload", {
	# "sid": fields.String(required=False, min_length=1, max_length=100, example="ACdbd8ccbba6341c5a8e1d4c1877659598"),
	# "from": fields.String(required=False, min_length=1, max_length=100, example="50505"),
	# "auth_token": fields.String(required=False, min_length=1, max_length=100, example="48ca09f7df13632efec3d1ac52beb80f"),
	# "api_url": fields.String(required=False, max_length=255),
	# "user_id": fields.Integer(required=False, min_length=1, max_length=100, example="test"),
	# "username": fields.String(required=False, min_length=1, max_length=100, example="testuser"),
	# "password": PasswordField(required=False, min_length=1, max_length=100),
	# "provider_type": fields.Integer(required=True),
	"message_type": fields.String(required=True, min_length=1, max_length=100, example="API/CAMPAIGN"),
	"messages": fields.List(fields.Nested(SendSMSBodySchema, required=True, skip_none=True), skip_none=True)
})


TwilioInboundQueryParams = reqparse.RequestParser()
TwilioInboundQueryParams.add_argument("SmsMessageSid", required=True, help="SmsMessageSid is missing")
TwilioInboundQueryParams.add_argument("NumMedia", required=False)
TwilioInboundQueryParams.add_argument("SmsSid", required=True, help="SmsSid is missing")
TwilioInboundQueryParams.add_argument("To", required=True, help="To Number is missing")
TwilioInboundQueryParams.add_argument("ToCountry", required=True, help="ToCountry is missing")
TwilioInboundQueryParams.add_argument("ToState", required=True, help="ToState is missing")
TwilioInboundQueryParams.add_argument("ToCity", required=True, help="ToCity is missing")
TwilioInboundQueryParams.add_argument("ToZip", required=True, help="ToZip is missing")
TwilioInboundQueryParams.add_argument("From", required=True, help="From is missing")
TwilioInboundQueryParams.add_argument("FromCountry", required=True, help="FromCountry is missing")
TwilioInboundQueryParams.add_argument("FromState", required=True, help="FromState is missing")
TwilioInboundQueryParams.add_argument("FromCity", required=True, help="FromCity is missing")
TwilioInboundQueryParams.add_argument("FromZip", required=True, help="FromZip is missing")
TwilioInboundQueryParams.add_argument("SmsStatus", required=True, help="SmsStatus is missing")
TwilioInboundQueryParams.add_argument("Body", required=True, help="Body is missing")
TwilioInboundQueryParams.add_argument("MessagingServiceSid", required=True, help="MessagingServiceSid is missing")
TwilioInboundQueryParams.add_argument("NumSegments", required=True, help="NumSegments is missing")
TwilioInboundQueryParams.add_argument("MessageSid", required=True, help="MessageSid is missing")
TwilioInboundQueryParams.add_argument("AccountSid", required=True, help="AccountSid is missing")
TwilioInboundQueryParams.add_argument("ApiVersion", required=False)


@api.route("send")
class SendSMS(Resource):
	
	@api.expect(SendSMSPayload, skip_none=True, validate=True)
	@api.response(200, "Send SMS", SendSMSResponse)
	@api.marshal_with(SendSMSResponse, skip_none=True)
	def post(self):
		"""
		send SMS using HTTP API
		"""
		validate_custom_fields_in_payload(api.payload, SendSMSPayload)
		payload = api.payload
		
		connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
		channel = connection.channel()
		# timestamp = time.time()
		# now = datetime.datetime.now()
		# expire = 1000 * int((now.replace(hour=23, minute=59, second=59, microsecond=999999) - now).total_seconds())
		response_dict = dict()
		request_id = str(uuid4())
		response_dict['request_id'] = request_id
		response_dict['messages'] = []
		
		if payload.get('messages') and len(payload['messages']) > 0:
			
			# create/declare a queue
			message_type = payload['message_type'] if payload.get('message_type') else None
			# channel.queue_declare(queue=queue_name)
			
			# # user_id = payload['user_id']
			# from_number = payload['from'] if payload.get('from') else None
			# # provider_type = payload['provider_type']
			# api_url = payload['api_url'] if payload.get('api_url') else None
			# username = payload['username'] if payload.get('username') else None
			# password = payload['password'] if payload.get('password') else None
			# auth_token = payload['auth_token'] if payload.get('auth_token') else None
			# sid = payload['sid'] if payload.get('sid') else None
			
			for msg in payload['messages']:
				msgid = str(uuid4())

				queue_name = msg['sid'] if msg.get('sid') else 'default'
				channel.queue_declare(queue=queue_name)
			
				item = dict()
				item['msgid'] = msgid
				# item['request_id'] = request_id
				item['to'] = msg['to_number']
				item['from'] = msg['from'] if msg.get('from') else None
				item['sid'] = queue_name 
				# item['user_id'] = user_id            
				# item['sms_cost'] = msg['sms_cost']
				# item['api_url'] = api_url 
				item['body'] = msg['content']
				item['message_type'] = message_type 
				# item['username'] = username
				# item['password'] = password
				# item['auth_token'] = auth_token 
				response_dict['messages'].append({
					'msgid': msgid,
					'from': msg['from'],
					'to_number': msg['to_number'],
					'content': msg['content'],
				})
				logger.info("messages sent to outbound queue: [%s]" %(str(item),) )
				# publish a message to the queue
				channel.basic_publish(exchange='', routing_key=queue_name, body=json.dumps(item))
		
		# close the connection
		connection.close()

		try:
			response = {
				"status": "Success",
				"code": "200",
				"message": "Messages sent successfully",
				"data": response_dict
			}
			return response
		finally:
			pass


@api.route("receive")
class ReceiveSMS(Resource):
	
	# @api.expect(SendSMSPayload, skip_none=True, validate=True)
	# @api.response(200, "Send SMS", SendSMSResponse)
	# @api.marshal_with(SendSMSResponse, skip_none=True)
	@api.expect(TwilioInboundQueryParams, validate=True, skip_none=True)
	def post(self):
		"""
		Receive SMS using HTTP API
		"""
		# validate_custom_fields_in_payload(api.payload, SendSMSPayload)
		query_params = TwilioInboundQueryParams.parse_args()
		
		connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
		channel = connection.channel()
		
		# payload = api.payload
		# print(query_params)
		logger.info("messages sent to inbound queue: [%s]" %(str(query_params),) )
		# publish a message to the queue
		channel.basic_publish(exchange='', routing_key="inbound-sms", body=json.dumps(query_params))

		try:
			response = {
				"status": "Success",
				"code": "200",
				"message": "Message Received successfully",
				"data": {}
			}
			return response
		finally:
			pass


