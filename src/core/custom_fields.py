import re
from flask import jsonify, make_response
from flask_restplus import fields, abort
from .constants import ArgumentTypes

EMAIL_REGEX = re.compile(r'\S+@\S+\.\S+')
PHONE_REGEX = re.compile(r'[0-9]{5,15}')


class CustomField(fields.String):
    pass


class PasswordField(CustomField):
    """
    Password field
    """
    __schema_type__ = "string"
    __schema_format__ = "password"
    __schema_example__ = "H7jiL2f$a67"
    __help__ = "Password should be at least 8 characters in length"

    def validate(self, value):
        if not value:
            return False
        if value:
            value = value.strip()
        # if len(value) < 8:
        #     return False
        return True


class EmailField(CustomField):
    """
    Email field
    """
    __schema_type__ = "string"
    __schema_format__ = "email"
    __schema_example__ = "email@domain.com"
    __help__ = "Invalid Email format"

    def validate(self, value):
        if not value:
            return False
        if value:
            value = value.strip()
        if not EMAIL_REGEX.match(value):
            return False
        return True


class PhoneNumberField(CustomField):
    __schema_type__ = "string"
    __schema_format__ = "phone number"
    __schema_example__ = "6149569245"
    __help__ = "Phone number should only have [0,1..9] letters"

    def validate(self, value):
        if not value:
            return False
        if value:
            value = value.strip()
        if not PHONE_REGEX.match(value):
            return False
        return True


def validate_custom_fields_in_payload(payload, api_model):
    """
    Validate payload against an api_model. Aborts in case of failure
    - This function is for custom fields as they can"t be validated by
      flask restplus automatically.
    - This is to be called at the start of a post or put method
    """
    # check if any reqd fields are missing in payload
    for key in api_model:
        if api_model[key].required and key not in payload:
            abort(400, "Required field \"%s\" missing" % key)
    # check payload
    for key in payload:
        field = api_model[key]
        if isinstance(field, fields.List):
            field = field.container
            data = payload[key]
        else:
            data = [payload[key]]
        if isinstance(field, CustomField) and hasattr(field, "validate"):
            for i in data:
                if not field.validate(i):
                    abort(400, "Validation of \"%s\" field failed" % key,
                          errors={key: field.__help__})


def validate_args(values, fields):
    error = dict()
    for field in fields:
        value = values.get(field)
        if value:
            if field == ArgumentTypes.EMAIL:
                if not EMAIL_REGEX.match(value):
                    error[field] = "Invalid Email Address"
            elif field == ArgumentTypes.PHONE:
                if not PHONE_REGEX.match(value):
                    error[field] = "Invalid Phone Number"
            
        else:
            error[field] = "'{}' is required".format(field)
    if error:
        abort(make_response(jsonify(errors=error, message='Input payload validation failed', statusCode=400), 400))
