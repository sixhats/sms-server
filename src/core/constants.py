RESPONSE_STATUSES = {
    '200': 'Success',
    '409': 'BUSINESS_RULE_FAILURE',
    '404': 'NOT_FOUND'
}

ROLE_TYPES = {'CUSTOM': 'CUSTOM', 'BUILTIN': 'BUILTIN'}

class ArgumentTypes:
    EMAIL = 'email'
    PHONE = 'phone'
