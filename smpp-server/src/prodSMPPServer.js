var smpp = require('smpp');
var axios = require('axios');

const hostname = '0.0.0.0';
const port = 2775;

// This should of course be replaced with your preferred auth system
function checkAsyncUserPass(username, password, cb) {
    if (username === 'smppclient' && password === 'password') {
        // The last parameter is just user meta data that will be attached to the session as "userData" and is optional
        cb(null, true, {'username': 'smppclient', 'password': 'password'});
    } else {
        cb(null, false);
    }
}


var server = smpp.createServer((session) => {
	console.log('create server successs');
	
	session.on('bind_transceiver', function(pdu) {
		
		console.log("bind_transceiver success", pdu.command_status);
		
		checkAsyncUserPass(pdu.system_id, pdu.password, function(err) {
			if (err) {
				console.log('SMPP bind transceiver error: ' + pdu.command_status);
				session.send(pdu.response({
					command_status: smpp.ESME_RBINDFAIL
				}));
				session.close();
				return;
			}else{
				console.log('Connected');
				session.send(pdu.response());
				session.resume();
			}
		});
	});

	session.on('pdu', function(pdu){
		// console.log('pdu triggered', pdu.command, pdu.sequence_number);
		if (pdu.command == 'submit_sm') {
			var fromNumber = pdu.source_addr.toString();
			var toNumber = pdu.destination_addr.toString();
			var text = '';
			if (pdu.short_message && pdu.short_message.message) {
				text = pdu.short_message.message;
			}
			// MMD Twilio SID AC3fb536f005727035ce3d03da876455d3
			// MMD Twilio Password 7debe607f14c0ad76eca9942c9fa6975
			// MMD Twilio Messaging service ID MGa4270fdc9d529b90e641828114cfe913
			let data = {
				"sid": "AC3fb536f005727035ce3d03da876455d3",
				"from": fromNumber,
				"auth_token": "7debe607f14c0ad76eca9942c9fa6975",
				"provider_type": "TWILIO",
				"messages": [
				  {
					"to_number": toNumber,
					"content": text,
					"msg_service_id": "MGb31a1a892dfade244710cffa028d6011"
				  }
				]
			}
			console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);
      		axios.post('http://159.89.139.170:6543/send', data).then(res => {
				if(res.data && res.data.data && res.data.data.messages){
					res.data.data.messages.forEach(element => {
						console.log('message sent to http://159.89.139.170:6543/send msgid: '+element.msgid);
						// Reply to SMSC that we received and processed the SMS
						session.submit_sm_resp({ 
							command_status: smpp.ESME_ROK,
							message_id: element.msgid,
							sequence_number: pdu.sequence_number,
						});
					});
				}
			}).catch(error => {
				console.error(error)
			})
		}
	})

	// Respond to an enquire link
	session.on('enquire_link', function (pdu) {
		session.send(pdu.response());
		// console.log('Responded an enquire link', pdu.command_status);
	});

	// Response to an enquire link
	session.on('enquire_link_resp', function (pdu) {
		// console.log('Received a response to an enquire link', pdu.command_status);
	});

	// Listen to the unbind event and kill the process
	session.on('unbind', function(pdu) {
		session.send(pdu.response());
		console.log('SMPP unbind', pdu.command_status);
	});

	// Kill the process when an SMPP connection is closed
	session.on('close', function () {
		console.log('SMPP connection is closed');
	});

	session.on('error', error => { 
		console.log('smpp error', error.message)   
	});

});


server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});